

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js")

router.get("/viewTasks", (req, res) => {
	taskController.getAllTask().then(result => res.send(result));
})

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})

router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(result => res.send(result));
})

router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;